from flask import render_template, request, Blueprint
from localuwc.models import Post

main = Blueprint('main', __name__)


@main.route("/")
@main.route("/home")
def home():
    return render_template('home.html', title='Home')

@main.route("/home2")
def home4():
    return render_template('home2.html', title='Home2')

#MM-23: UWC Map Enhancement Committed by Pi
@main.route('/map')
def map():
    return render_template('map.html',title='Map Test by Pi')

@main.route("/about")
def about():
    return render_template('about.html', title='About')

@main.route("/uwcinfo")
def uwcinfo():
    return render_template('uwcinfo.html', title='UWC')

@main.route("/apply")
def apply():
    return render_template('apply.html', title='Apply')

@main.route("/nav")
def navtest():
    return render_template('navtest.html', title='Nav-Testing')

@main.route("/alumni")
def alumni():
    return render_template('alumni.html', title='Alumni')

@main.route("/uwcerblog")
def uwcerblog():
	page = request.args.get('page', 1, type=int)
	posts = Post.query.order_by(Post.date_posted.desc()).paginate(page=page, per_page=5)
	return render_template('uwcerblog.html',posts=posts)